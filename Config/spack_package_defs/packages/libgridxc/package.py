from spack import *


class Libgridxc(CMakePackage):
    """A library for exchange-correlation calculations on radial and
       solid-state grids"""

    homepage = "https://gitlab.com/siesta-project/libraries/libgridxc"

    git = 'https://gitlab.com/siesta-project/libraries/libgridxc.git'

    version("2.0.0", commit="77058d35f5f4")   # using commit is 'trusted'
    version("2.0.1", commit="8311c3ec1648")   

    variant('mpi', default=False, description='Use MPI')
    variant('libxc', default=False, description='Use libxc')
    variant('gridsp', default=False, description='Use single-precision grid operations')
    

    depends_on('cmake@3.14.0:', type='build')

    depends_on('mpi', when='+mpi')
    depends_on('libxc@4:5', when='+libxc')
    

    def cmake_args(self):
       args = [
            self.define_from_variant('WITH_MPI', 'mpi'),
            self.define_from_variant('WITH_LIBXC', 'libxc'),
            self.define_from_variant('WITH_GRID_SP', 'gridsp'),
       ]

       return args
