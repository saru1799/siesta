# Copyright 2013-2022 Lawrence Livermore National Security, LLC and other
# Spack Project Developers. See the top-level COPYRIGHT file for details.
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)

from spack.package import *

# See the Spack documentation for more information on packaging.
# ----------------------------------------------------------------------------

# Package updated (wip) by Alberto Garcia for modern versions and CUDA support

class Elsi(CMakePackage, CudaPackage):
    """ELSI provides a unified interface for electronic structure
    codes to a variety of eigenvalue solvers."""

    homepage = "https://wordpress.elsi-interchange.org/"
    git = 'https://gitlab.com/elsi_project/elsi_interface.git'

    version('2.10.1', commit='bca7e256')

    # Variants (translation of cmake options)
    variant("add_underscore", default=False, description="Suffix C functions with an underscore")

    variant("cuda", default=False, description="Enable CUDA for GPU acceleration")
    
    variant(
        "elpa2_kernel",
        default="none",
        description="ELPA2 Kernel",
        values=("none", "AVX", "AVX2", "AVX512"),
        multi=False,
    )

    variant("enable_pexsi", default=False, description="Enable PEXSI support")
    variant("enable_sips", default=False, description="Enable SLEPc-SIPs support")
    variant("use_external_elpa", default=False, description="Build ELPA using SPACK")
    variant("use_external_ntpoly", default=False, description="Build NTPoly using SPACK")

    # Basic dependencies
    depends_on("blas", type="link")
    depends_on("lapack", type="link")
    depends_on("cmake", type="build")
    depends_on("mpi")
    depends_on("scalapack", type="link")

    # Library dependencies
    depends_on("elpa@2020.05.001~openmp", when="+use_external_elpa")
    depends_on("ntpoly", when="+use_external_ntpoly")
    depends_on("slepc", when="+enable_sips")
    depends_on("petsc", when="+enable_sips")

    conflicts("cuda_arch=none", when="+cuda", msg="CUDA architecture is required")
    
    patch("mpi+gnu.patch")
    
    def cmake_args(self):
        from os.path import dirname

        spec = self.spec
        args = []

        # Compiler Information
        # (ELSI wants these explicitly set)
        args += ["-DCMAKE_Fortran_COMPILER=" + self.spec["mpi"].mpifc]
        args += ["-DCMAKE_C_COMPILER=" + self.spec["mpi"].mpicc]
        args += ["-DCMAKE_CXX_COMPILER=" + self.spec["mpi"].mpicxx]

        # Handle the various variants
        if "+add_underscore" in self.spec:
            args += ["-DADD_UNDERSCORE=ON"]
        if (
            "elpa2_kernel" in self.spec.variants
            and self.spec.variants["elpa2_kernel"].value != "none"
        ):
            kernel = self.spec.variants["elpa2_kernel"].value
            args += ["-DELPA2_KERNEL=" + kernel]


        if "+cuda" in self.spec:
             # Set up the cuda macros needed by the build
             args.append("-DUSE_GPU_CUDA=ON")
             cuda_arch_list = self.spec.variants["cuda_arch"].value
             cuda_arch = cuda_arch_list[0]
             if cuda_arch != "none":
                 args.append(f"-DCMAKE_CUDA_FLAGS=-arch=sm_{cuda_arch} -O3 -std=c++11")
                 args.append(f"-DCMAKE_CUDA_ARCHITECTURES={cuda_arch}")
             
        if "+enable_pexsi" in self.spec:
            args += ["-DENABLE_PEXSI=ON"]

        if "+enable_sips" in self.spec:
            args += ["-DENABLE_SIPS=ON"]

        if "+use_external_elpa" in self.spec:
            args += ["-DUSE_EXTERNAL_ELPA=ON"]
            # Setup the searchpath for elpa
            elpa = self.spec["elpa"]
            elpa_module = find(elpa.prefix, "elpa.mod")
            args += ["-DINC_PATHS=" + dirname(elpa_module[0])]

        if "+use_external_ntpoly" in self.spec:
            args += ["-DUSE_EXTERNAL_NTPOLY=ON"]

        # Only when using fujitsu compiler
        if self.spec.satisfies("%fj"):
            args += ["-DCMAKE_Fortran_MODDIR_FLAG=-M"]

        return args
