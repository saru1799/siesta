from spack import *


class Pexsi(CMakePackage):

    """The PEXSI library is written in C++, and uses message passing interface
    (MPI) to parallelize the computation on distributed memory computing
    systems and achieve scalability on more than 10,000 processors.

    The Pole EXpansion and Selected Inversion (PEXSI) method is a fast
    method for electronic structure calculation based on Kohn-Sham density
    functional theory. It efficiently evaluates certain selected elements
    of matrix functions, e.g., the Fermi-Dirac function of the KS Hamiltonian,
    which yields a density matrix. It can be used as an alternative to
    diagonalization methods for obtaining the density, energy and forces
    in electronic structure calculations.
    """

    homepage = "https://math.berkeley.edu/~linlin/pexsi/index.html"

    url = "https://bitbucket.org/berkeleylab/pexsi/downloads/pexsi_v2.0.0.tar.gz"
    version("2.0.0",sha256="c5c83c2931b2bd0c68a462a49eeec983e78b5aaa1f17dd0454de4e27b91ca11f")

    depends_on("parmetis")
    depends_on("superlu-dist@7.2.0")   # Version 8 not working

    variant("fortran", default=True, description="Builds the Fortran interface")
    variant('openmp', default=False, description='Build with OpenMP support')

    depends_on('cmake@3.17.0:', type='build')

    # generator = 'Ninja'
    # depends_on('ninja', type='build')
    
    depends_on('lapack')
    depends_on('mpi')

    def cmake_args(self):
       args = [
            self.define_from_variant('PEXSI_ENABLE_FORTRAN', 'fortran'),
            self.define_from_variant('PEXSI_ENABLE_OPENMP', 'openmp'),

       ]

       return args

