from spack import *


class Xmlf90(CMakePackage):
    """XML package for Fortran."""

    homepage = "https://gitlab.com/siesta-project/libraries/xmlf90"

    git = 'https://gitlab.com/siesta-project/libraries/xmlf90.git'

    version("1.6.3", commit="963fe5d1148723")   # using commit is 'trusted'
    
    depends_on('cmake@3.14.0:', type='build')

    def cmake_args(self):
       args = []

       return args
