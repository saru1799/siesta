# 
 
siesta_subtest(fe_spin LABELS fe polarized)
siesta_subtest(fe_spin_directphi LABELS fe polarized)
siesta_subtest(fe_noncol)
siesta_subtest(fe_noncol_gga)
siesta_subtest(fe_noncol_kp)
siesta_subtest(fe_noncol_sp)
