#
siesta_subtest(wannier EXTRA_FILES wannier.nnkp LABELS simple)

if(WITH_WANNIER90)
  siesta_subtest(w90-on-the-fly LABELS long)
  siesta_subtest(graphene_w90 IS_SIMPLE LABELS simple)

  if (num_ranks GREATER 2)
    set(small_test_nprocs 2)
  else()
    set(small_test_nprocs ${num_ranks})
  endif()
  siesta_subtest(w90-chem-h2 LABELS simple MPI_NPROC ${small_test_nprocs})
endif()


