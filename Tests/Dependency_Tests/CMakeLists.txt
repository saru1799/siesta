
if(WITH_DFTD3)
  siesta_subtest(dftd3 IS_SIMPLE LABELS simple)
endif()

# This test might fail if LUA is not present.
if(WITH_FLOOK)
  siesta_subtest(lua_h2o EXTRA_FILES siesta.lua)
endif()